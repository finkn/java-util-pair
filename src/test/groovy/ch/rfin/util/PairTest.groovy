package ch.rfin.util;

import spock.lang.*;

import java.util.function.*;
import static ch.rfin.util.Pair.pair;

class PairTest extends Specification {

  @Unroll
  def "testing get_1 and get_2 for pair (#_1, #_2)"() {
    given:
      Pair p = pair(_1, _2)
    expect:
      p.get_1() == _1
      p.get_2() == _2
    where:
      _1    |   _2
      1     |   "a"
      "b"   |   2
  }

  @Unroll
  def "(#_1, #_2).toString() should return #str"() {
    given:
      Pair p = pair(_1, _2)
    expect:
      p.toString() == str
    where:
      _1      | _2       | str
      1       | 2        | "(1, 2)"
      "hello" | "world"  | "(hello, world)"
      null    | 2        | "(null, 2)"
      1       | null     | "(1, null)"
  }

  def "a pair should not be equal to a non-pair"() {
    expect:
      pair(1,2) != "hello"
  }

  def "a pair should not be equal to a pair with unequal elements"() {
    expect:
      pair(1,2) != pair(2,2)
      pair(1,2) != pair(1,1)
      pair(1,2) != pair(2,1)
      pair(1,2) != pair(null,2)
      pair(1,2) != pair(1,null)
      pair(1,2) != pair(null,null)
  }

  def "a pair should be equal to a pair with equal elements"() {
    expect:
      pair(1,2) == pair(1,2)
      pair(null,2) == pair(null,2)
      pair(1,null) == pair(1,null)
      pair(null,null) == pair(null,null)
  }

  def "(a,b).with_1(c) should return (c,b)"() {
    given:
      Pair p = pair(1,2)
    when:
      Pair q = p.with_1(3)
    then:
      p != q
      q == pair(3,2)
  }

  def "(a,b).with_2(c) should return (a,c)"() {
    given:
      Pair p = pair(1,2)
    when:
      Pair q = p.with_2(3)
    then:
      p != q
      q == pair(1,3)
  }

  def "(a,b).swap() should return (b,a)"() {
    given:
      Pair p = pair(1,2)
    when:
      Pair q = p.swap()
    then:
      p != q
      q == pair(2,1)
  }

  def "(a,b).map_1(f) should return (f(a),b)"() {
    given:
      Pair p = pair(1,2)
      Function f = { it*3 }
    when:
      Pair q = p.map_1(f)
    then:
      q == pair(3,2)
  }

  def "(a,b).map_2(f) should return (a,f(b))"() {
    given:
      Pair p = pair(1,2)
      Function f = { it*3 }
    when:
      Pair q = p.map_2(f)
    then:
      q == pair(1,6)
  }

  def "(a,b).map(f,g) should return (f(a),g(b))"() {
    given:
      Pair p = pair(1,2)
      Function f = { it*3 }
      Function g = { it+2 }
    when:
      Pair q = p.map(f,g)
    then:
      q == pair(3,4)
  }

  def "(a,b).apply(biFun) should return biFun.apply(a,b)"() {
    given:
      Pair p = pair(1,2)
      BiFunction f = { a,b -> a + b }
    expect:
      p.apply(f)
  }

  def "(a,b).test(biPred) should return biPred.test(a,b)"() {
    given:
      BiPredicate f = { a,b -> a > b }
    expect:
      !pair(1,2).test(f) // !
      pair(2,1).test(f)
  }

  def "(a,b).accept(biConsumer) should return biConsumer.accept(a,b)"() {
    given:
      List list = []
      Pair p = pair(1,2)
      BiConsumer f = { a,b -> list.add(a); list.add(b) }
    when:
      p.accept(f)
    then:
      list == [1,2]
  }

  // TODO: Generate random pairs and check that equal pairs produce equal hash codes.

}
