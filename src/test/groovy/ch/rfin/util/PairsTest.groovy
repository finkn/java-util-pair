package ch.rfin.util;

import spock.lang.*;
import java.util.function.*;
import static ch.rfin.util.Pair.pair;

class PairsTest2 extends Specification {

  def "pair from map entry"() {
    given:
      Map.Entry entry = ["a": 1].entrySet().asList().get(0)
    expect:
      Pairs.pairFrom(entry) == Pair.of("a", 1)
  }

  def "pair from list"() {
    given:
      List list = [1,2]
    expect:
      Pairs.pairFrom(list) == Pair.of(1,2)
  }

  def "pairs from list"() {
    given:
      List list = [1,2,3,4,5,6]
    expect:
      Pairs.pairsFrom(list) == [Pair.of(1,2), Pair.of(3,4), Pair.of(5,6)]
  }

  def "pairs from empty list"() {
    given:
      List list = []
    expect:
      Pairs.pairsFrom(list) == []
  }

  def "pairs from map"() {
    given:
      Map map = [a:1, b:2, c:3]
    expect:
      Pairs.pairsFrom(map) == [Pair.of('a', 1), Pair.of('b', 2), Pair.of('c', 3)]
  }

  def "pairs from empty map"() {
    given:
      Map map = [:]
    expect:
      Pairs.pairsFrom(map) == []
  }

  def "unary function from binary function"() {
    given:
      BiFunction biFun = { a,b -> a + b }
      Function fun = Pairs.function(biFun)
    expect:
      fun.apply(pair(1,2)) == 3
      fun.apply(pair(4,3)) == 7
  }

  def "unary predicate from binary predicate"() {
    given:
      BiPredicate biPred = { a,b -> a > b }
      Predicate pred = Pairs.predicate(biPred)
    expect:
      !pred.test(pair(1,2))
      pred.test(pair(2,1))
  }

  def "unary consumer from binary consumer"() {
    given:
      List list = []
      BiConsumer biCon = { a,b -> list.add(a); list.add(b) }
      Consumer con = Pairs.consumer(biCon)
    when:
      con.accept(pair(1,2))
      con.accept(pair(4,3))
    then:
      list == [1,2,4,3]
  }

  def "binary function from unary function"() {
    given:
      Function fun = { Pair p -> p._1 + p._2 }
      BiFunction biFun = Pairs.biFunction(fun)
    expect:
      biFun.apply(1,2) == 3
      biFun.apply(4,3) == 7
  }

  def "binary predicate from unary predicate"() {
    given:
      Predicate pred = { Pair p -> p._1 > p._2 }
      BiPredicate biPred = Pairs.biPredicate(pred)
    expect:
      !biPred.test(1,2)
      biPred.test(2,1)
  }

  def "binary consumer from unary consumer"() {
    given:
      List list = []
      Consumer con = { Pair p -> list.add(p._1); list.add(p._2) }
      BiConsumer biCon = Pairs.biConsumer(con)
    when:
      biCon.accept(1,2)
      biCon.accept(4,3)
    then:
      list == [1,2,4,3]
  }

  @Unroll
  def "stream(#xs) == #expected"() {
    expect:
      Pairs.stream(xs).toList() == expected
    where:
      xs            |   expected
      []            |   []
      [3]           |   []
      [3,1]         |   [pair(3,1)]
      [3,1,-2]      |   [pair(3,1)]
      [3,1,-2,1]    |   [pair(3,1), pair(-2,1)]
  }

  @Unroll
  def "stream(#map) == #expected"() {
    expect:
      Pairs.stream(map).toSet() == expected
    where:
      map             |   expected
      []              |   [] as Set
      [a:3]           |   [pair('a',3)] as Set
      [a:3,b:1]       |   [pair('a',3), pair('b', 1)] as Set
      [a:3,b:1,c:-2]  |   [pair('a',3), pair('b', 1), pair('c', -2)] as Set
  }

  @Unroll
  def "enumerate(#xs) == #expected"() {
    expect:
      Pairs.enumerate(xs).toList() == expected
    where:
      xs        |   expected
      []        |   []
      [3]       |   [pair(0,3)]
      [3,1]     |   [pair(0,3), pair(1,1)]
      [3,1,-2]  |   [pair(0,3), pair(1,1), pair(2, -2)]
  }

  @Unroll
  def "streamAdjacent(#xs) == #expected"() {
    expect:
      Pairs.streamAdjacent(xs).toList() == expected
    where:
      xs        |   expected
      []        |   []
      [3]       |   []
      [3,1]     |   [pair(3,1)]
      [3,1,2]   |   [pair(3,1), pair(1,2)]
      [3,1,2,0] |   [pair(3,1), pair(1,2), pair(2,0)]
  }

  @Unroll
  def "toMap(#ps) == #expected"() {
    expect:
      Pairs.toMap(ps) == expected
    where:
      ps                            |   expected
      []                            |   [:]
      [pair('a',3)]                 |   [a:3]
      [pair('a',3), pair('b',1)]    |   [a:3, b:1]
  }

  @Unroll
  def "zip(#xs, #ys) -> #expected"() {
    expect:
      Pairs.zip(xs, ys).toList() == expected
    where:
      xs     | ys       |  expected
      []     | []       |  []
      [1]    | []       |  []
      []     | [2]      |  []
      [1]    | [2]      |  [pair(1,2)]
      [1]    | [2,3]    |  [pair(1,2)]
      [1,3]  | [2]      |  [pair(1,2)]
      [1,3]  | [2,4]    |  [pair(1,2), pair(3,4)]
  }

  def "test_1"() {
    given:
      Predicate greaterThan3 = { n -> n > 3 }
      Predicate pred = Pairs.test_1(greaterThan3)
    expect:
      !pred.test(pair(1,1))
      !pred.test(pair(1,5))
      pred.test(pair(5,1))
      pred.test(pair(5,5))
  }

  def "test_2"() {
    given:
      Predicate greaterThan3 = { n -> n > 3 }
      Predicate pred = Pairs.test_2(greaterThan3)
    expect:
      !pred.test(pair(1,1))
      pred.test(pair(1,5))
      !pred.test(pair(5,1))
      pred.test(pair(5,5))
  }

  def "compare"() {
    given:
      Pair p11 = pair(1, 1)
      Pair p12 = pair(1, 2)
      Pair p21 = pair(2, 1)
      Pair p22 = pair(2, 2)
    expect:
      Pairs.compare(p11, p11) == 0
      Pairs.compare(p12, p12) == 0
      Pairs.compare(p21, p21) == 0
      Pairs.compare(p22, p22) == 0
      Pairs.compare(p11, p12) < 0
      Pairs.compare(p11, p21) < 0
      Pairs.compare(p11, p22) < 0
      Pairs.compare(p12, p21) < 0
      Pairs.compare(p12, p22) < 0
      Pairs.compare(p21, p22) < 0
      Pairs.compare(p12, p11) > 0
      Pairs.compare(p21, p11) > 0
      Pairs.compare(p22, p11) > 0
      Pairs.compare(p21, p12) > 0
      Pairs.compare(p22, p12) > 0
      Pairs.compare(p22, p21) > 0
  }

  def "pairWithFirst"() {
    given:
      def first = 3
      def withFirst = Pairs.pairWithFirst(first)
      def xs = [1, 2, 3, 4]
    expect:
      xs.stream().map(withFirst).toList() == [pair(3,1), pair(3,2), pair(3,3), pair(3,4)]
  }

  def "pairWithSecond"() {
    given:
      def second = 3
      def withSecond = Pairs.pairWithSecond(second)
      def xs = [1, 2, 3, 4]
    expect:
      xs.stream().map(withSecond).toList() == [pair(1,3), pair(2,3), pair(3,3), pair(4,3)]
  }

}
