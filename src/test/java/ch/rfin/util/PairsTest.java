package ch.rfin.util;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.util.function.*;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class PairsTest {

    @Test
    public void pairsFrom_iterable() {
        final Iterable<Integer> xs = List.of(3,4);
        final List<Pair<Integer,Integer>> result = Pairs.pairsFrom(xs);
        final List<Pair<Integer,Integer>> expected = List.of(Pair.of(3,4));
        assertEquals(expected, result);
    }

    @Test
    public void pairsFrom_map() {
        final Map<String,Integer> map = Map.of("hello", 3, "world", 4);
        final List<Pair<String,Integer>> result = Pairs.pairsFrom(map);
        final List<Pair<String,Integer>> expected = List.of(Pair.of("hello",3), Pair.of("world",4));
        assertEquals(new HashSet<>(expected), new HashSet<>(result));
    }

    @Test
    public void withId_should_return_x_as_first_and_y_second() {
        final Function<String,Pair<String,Integer>> f = Pairs.withId(String::length);
        final Pair<String,Integer> result = f.apply("hello");
        final Pair<String,Integer> expected = Pair.of("hello", 5);
        assertEquals(expected, result);
    }

    @Test
    public void constructing_stream_from_iterable_should_be_lazy() {
        final Iterable<Integer> xs = () -> new ThrowingIterator<>();
        Pairs.stream(xs); // Should do nothing.
    }

    @Test
    public void constructing_stream_of_adjacent_from_iterable_should_be_lazy() {
        final Iterable<Integer> xs = () -> new ThrowingIterator<>();
        Pairs.streamAdjacent(xs); // Should do nothing.
    }

    @Test
    public void enumerating_iterable_should_be_lazy() {
        final Iterable<Integer> xs = () -> new ThrowingIterator<>();
        Pairs.enumerate(xs); // Should do nothing.
    }

    @Test
    public void stream_iterable() {
        final Iterable<Integer> xs = List.of(1,2,3,4);
        final List<Pair<Integer,Integer>> result = Pairs.stream(xs).collect(toList());
        final List<Pair<Integer,Integer>> expected = List.of(Pair.of(1,2), Pair.of(3,4));
        assertEquals(expected, result);
    }

    @Test
    public void stream_adjacent_iterable() {
        final Iterable<Integer> xs = List.of(1,2,3,4);
        final List<Pair<Integer,Integer>> result = Pairs.streamAdjacent(xs).collect(toList());
        final List<Pair<Integer,Integer>> expected = List.of(Pair.of(1,2), Pair.of(2,3), Pair.of(3,4));
        assertEquals(expected, result);
    }

    @Test
    public void stream_map() {
        final Map<String,Integer> map = Map.of("a", 1, "b", 2);
        final Set<Pair<String,Integer>> result = Pairs.stream(map).collect(toSet());
        final Set<Pair<String,Integer>> expected = Set.of(Pair.of("a",1), Pair.of("b",2));
        assertEquals(expected, result);
    }

    @Test
    public void enumerate_iterable() {
        final Iterable<String> xs = List.of("a", "b", "c");
        final List<Pair<Integer,String>> result = Pairs.enumerate(xs).collect(toList());
        final List<Pair<Integer,String>> expected = List.of(Pair.of(0,"a"), Pair.of(1,"b"), Pair.of(2,"c"));
        assertEquals(expected, result);
    }

    @Test
    public void zip() {
        final Iterable<String> xs = List.of("a", "b", "c");
        final Iterable<Integer> ys = List.of(1, 2, 3);
        final List<Pair<String,Integer>> result = Pairs.zip(xs, ys).collect(toList());
        final List<Pair<String,Integer>> expected = List.of(Pair.of("a",1), Pair.of("b",2), Pair.of("c",3));
        assertEquals(expected, result);
    }

    @Test
    public void pairs_to_map_collector() {
        final Map<Integer,Integer> result = Stream.of(Pair.of(1,2), Pair.of(2,3), Pair.of(3,4))
            .collect(Pairs.pairsToMap());
        final Map<Integer,Integer> expected = Map.of(1,2, 2,3, 3,4);
        assertEquals(expected, result);
    }

    @Test
    public void map_1() {
        final Function<Integer, Integer> plus3 = n -> n + 3;
        final Function<Pair<Integer,Integer>, Pair<Integer,Integer>> f = Pairs.map_1(plus3);
        assertEquals(Pair.of(4, 2), f.apply(Pair.of(1, 2)));
    }

    @Test
    public void map_2() {
        final Function<Integer, Integer> plus3 = n -> n + 3;
        final Function<Pair<Integer,Integer>, Pair<Integer,Integer>> f = Pairs.map_2(plus3);
        assertEquals(Pair.of(1, 5), f.apply(Pair.of(1, 2)));
    }

    @Test
    public void map() {
        final Function<Integer, Integer> plus1 = n -> n + 1;
        final Function<Integer, Integer> plus3 = n -> n + 3;
        final Function<Pair<Integer,Integer>, Pair<Integer,Integer>> f = Pairs.map(plus1, plus3);
        assertEquals(Pair.of(2, 5), f.apply(Pair.of(1, 2)));
    }

    @Test
    public void comparator() {
        final List<Pair<Integer,Integer>> ps = new ArrayList<>(List.of(Pair.of(1,3), Pair.of(2,1), Pair.of(1,2)));
        Collections.sort(ps, Pairs.comparator());
        final List<Pair<Integer,Integer>> expected = List.of(Pair.of(1,2), Pair.of(1,3), Pair.of(2,1));
        assertEquals(expected, ps);
    }
}

class ThrowingIterator<T> implements Iterator<T> {
    @Override
    public boolean hasNext() {
        return true;
    }
    @Override
    public T next() {
        throw new AssertionError("ThrowingIterator.next()");
    }
}
