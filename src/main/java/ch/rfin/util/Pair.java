package ch.rfin.util;

import java.util.Objects;
import java.util.function.*;

/**
 * A Pair of values. This class is immutable (and therefore thread safe) if
 * T1 and T2 are immutable.
 *
 * @author Christoffer Fink
 * @version 1.1.0
 */
public final class Pair<T1,T2> {

  public final T1 _1;
  public final T2 _2;

  private Pair(T1 first, T2 second) {
    this._1 = first;
    this._2 = second;
  }

  /** Factory method. */
  public static <T1,T2> Pair<T1,T2> of(final T1 first, final T2 second) {
    return new Pair<>(first, second);
  }

  /**
   * Special case factory method for making a duplicate pair
   * ({@code x -> (x,x)}).
   */
  public static <T> Pair<T,T> of(final T both) {
    return new Pair<>(both, both);
  }

  /**
   * More descriptive alias for {@link #of(Object, Object)}; useful for
   * static imports.
   */
  public static <T1,T2> Pair<T1,T2> pair(final T1 first, final T2 second) {
    return new Pair<>(first, second);
  }

  /**
   * More descriptive alias for {@link #of(Object)}; useful for
   * static imports.
   */
  public static <T> Pair<T,T> pair(final T both) {
    return new Pair<>(both, both);
  }

  /** {@code (a,b).get_1() = a}. **/
  public T1 get_1() {
    return _1;
  }

  /** {@code (a,b).get_2() = b}. **/
  public T2 get_2() {
    return _2;
  }

  /** {@code (a,b).with_1(c) = (c,b)} */
  public <U> Pair<U,T2> with_1(final U newFirst) {
    return of(newFirst, _2);
  }

  /** {@code (a,b).with_2(c) = (a,c)} */
  public <U> Pair<T1,U> with_2(final U newSecond) {
    return of(_1, newSecond);
  }

  /** {@code (a,b).swap() = (b,a)} */
  public Pair<T2,T1> swap() {
    return of(_2, _1);
  }

  /**
   * Allows {@code q = p.map_1(f);} instead of
   * {@code q = Pair.of(f.apply(p._1), p._2)}.
   */
  public <U> Pair<U,T2> map_1(final Function<? super T1, ? extends U> f) {
    return of(f.apply(_1), _2);
  }

  /**
   * Allows {@code q = p.map_2(f);} instead of
   * {@code q = Pair.of(p._1, f.apply(p._2))}.
   */
  public <U> Pair<T1,U> map_2(final Function<? super T2, ? extends U> f) {
    return of(_1, f.apply(_2));
  }

  /**
   * Allows {@code q = p.map(f, g);} instead of
   * {@code q = Pair.of(f.apply(p._1), g.apply(p._2))}.
   */
  public <U,R> Pair<U,R> map(final Function<? super T1, ? extends U> f, final Function<? super T2, ? extends R> g) {
    return of(f.apply(_1), g.apply(_2));
  }

  /**
   * Apply the binary function to this pair.
   * Unfortunately, java.util.function.BiFunction cannot take a pair of values.
   * So doing {@code f.apply(pair) instead of f.apply(pair._1, pair._2)} is
   * not possible. This at least allows {@code pair.apply(f)}.
   * @see ch.rfin.util.Pairs#function(BiFunction)
   */
  public <R> R apply(final BiFunction<? super T1, ? super T2, ? extends R> f) {
    return f.apply(_1, _2);
  }

  /**
   * Use the binary predicate to test this pair.
   * Unfortunately, java.util.function.BiPredicate cannot take a pair of values.
   * So doing {@code p.test(pair) instead of p.test(pair._1, pair._2)} is
   * not possible. This at least allows {@code pair.test(p)}.
   * @see ch.rfin.util.Pairs#predicate(BiPredicate)
   */
  public boolean test(final BiPredicate<? super T1, ? super T2> p) {
    return p.test(_1, _2);
  }

  /**
   * Use the binary consumer to consume this pair.
   * Unfortunately, java.util.function.BiConsumer cannot take a pair of values.
   * So doing {@code c.accept(pair) instead of c.accept(pair._1, pair._2)} is
   * not possible. This at least allows {@code pair.accept(c)}.
   * @see ch.rfin.util.Pairs#consumer(BiConsumer)
   */
  public void accept(final BiConsumer<? super T1, ? super T2> c) {
    c.accept(_1, _2);
  }

  @Override
  public String toString() {
    return "(" + _1 + ", " + _2 + ")";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (getClass() != o.getClass()) {
      return false;
    }
    Pair<?,?> that = (Pair)o;
    return Objects.deepEquals(_1, that._1) && Objects.deepEquals(_2, that._2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(_1, _2);
  }

}
