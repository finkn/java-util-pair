package ch.rfin.util;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

/**
 * Utilities for working with pairs.
 * <p>
 * Collections
 * <p>
 * <ul>
 *  <li>pairFrom(Map.Entry)</li>
 *  <li>pairFrom(Iterable)</li>
 *  <li>pairsFrom(Map.Entry)</li>
 *  <li>pairsFrom(Iterable)</li>
 *  <li>toMap(Iterable)</li>
 * </ul>
 *
 * Streams
 * <p>
 * <ul>
 *  <li>stream(Iterable)</li>
 *  <li>stream(Map)</li>
 *  <li>streamAdjacent(Iterable)</li>
 *  <li>zip(Iterable, Iterable)</li>
 *  <li>pairsToMap()</li>
 * </ul>
 *
 * Functions
 * <p>
 * <ul>
 *  <li>map_1(Function)</li>
 *  <li>map_2(Function)</li>
 *  <li>map(Function, Function)</li>
 *  <li>test_1(Predicate)</li>
 *  <li>test_2(Predicate)</li>
 *  <li>withId(Function)</li>
 *  <li>function(BiFunction)</li>
 *  <li>biFunction(Function)</li>
 *  <li>predicate(biPredicate)</li>
 *  <li>biPredicate(Predicate)</li>
 *  <li>consumer(biConsumer)</li>
 *  <li>biConsumer(Consumer)</li>
 * </ul>
 *
 * Misc
 * <p>
 * <ul>
 *  <li>compare(Pair, Pair)</li>
 *  <li>comparator()</li>
 * </ul>
 *
 * <p>
 * All methods that construct streams from iterables do so lazily, and so are
 * compatible with infinite iterables.
 * <p>
 * Documentation conventions: {@code […]} represents a collection of values
 * (such as a List, a Collection, an Iterable, a Stream). {@code (a,b)}
 * represents a pair. {@code (k:v)} represents a map entry.
 *
 * @author Christoffer Fink
 * @version 1.1.0
 */
public final class Pairs {

  private Pairs() { }

  /**
   * {@code (k:v) → (k,v)} - converts a Map.Entry to a key-value pair.
   * @param entry Map.Entry (k:v).
   * @return key-value pair (k,v).
   */
  public static <K,V> Pair<K,V> pairFrom(Map.Entry<K,V> entry) {
    return Pair.of(entry.getKey(), entry.getValue());
  }

  /**
   * {@code [x₁, x₂, …] → (x₁, x₂)} - converts a list to a pair. Ignores any
   * elements after the first two.
   * @param xs list of values [x₁, x₂, …].
   * @return a pair (x₁, x₂).
   */
  public static <T> Pair<T,T> pairFrom(List<T> xs) {
    return Pair.of(xs.get(0), xs.get(1));
  }

  /**
   * Converts a list to a list of pairs.
   * @return {@code [(x0, x1), (x2, x3), …]}.
   * @throws IllegalArgumentException if uneven number of items
   * @deprecated Since 1.1.0. Use {@link #pairsFrom(Iterable)} instead.
   */
  @Deprecated(forRemoval = true)
  public static <T> List<Pair<T,T>> pairs(List<T> items) {
    final int len = items.size();
    if (len == 0) {
      return Collections.emptyList();
    }
    if (len % 2 != 0) {
      throw new IllegalArgumentException(len + " is not an even number of items");
    }
    List<Pair<T,T>> result = new ArrayList<>(len/2);
    for (int i = 1; i < len; i+=2) {
      result.add(Pair.of(items.get(i-1), items.get(i)));
    }
    return result;
  }

  /**
   * Converts an iterable to a list of pairs.
   * @return {@code [(x₀, x₁), (x₂, x₃), …]}.
   * @since 1.1.0
   */
  public static <T> List<Pair<T,T>> pairsFrom(Iterable<T> xs) {
      return stream(xs).collect(Collectors.toList());
  }

  /**
   * Converts a Map to a list of pairs.
   * @return {@code [(x₀, x₁), (x₂, x₃), …]}.
   * @deprecated Since 1.1.0.
   */
  @Deprecated(forRemoval = true)
  public static <K,V> List<Pair<K,V>> pairs(Map<K,V> items) {
    return pairsFrom(items);
  }

  /**
   * Converts a Map to a list of pairs.
   * @return {@code [(k₀,v₀), (k₁,v₁), (k₂,v₂), …]}.
   */
  public static <K,V> List<Pair<K,V>> pairsFrom(final Map<K,V> map) {
    if (map.isEmpty()) {
      return Collections.emptyList();
    }
    return stream(map).collect(Collectors.toList());
  }

  /**
   * {@code [(k₀,v₀), (k₁,v₁), (k₂,v₂), …] → [(k₀:v₀), (k₁:v₁), (k₂:v₂), …]} -
   * Converts pairs to a Map.
   * @return a map {@code [(k₀:v₀), (k₁:v₁), (k₂:v₂), …]}.
   */
  public static <K,V> Map<K,V> toMap(Iterable<Pair<K,V>> items) {
    final Map<K,V> map = new HashMap<>();
    items.forEach(p -> p.accept(map::put));
    return map;
  }

  /**
   * {@code [x₀, x₁, x₂, …] → [(x₀,x₁), (x₂,x₃), (x₄,x₅), …]} -
   * Converts an iterable of values to a stream of pairs, consuming two at a time.
   * @param xs an iterable of values [x₀, x₁, x₂, …].
   * @return a stream {@code [(x₀,x₁), (x₂,x₃), (x₄,x₅), …]}.
   * @since 1.1.0
   */
  public static <T> Stream<Pair<T,T>> stream(Iterable<T> xs) {
    final Iterator<T> it = xs.iterator();
    if (!it.hasNext()) {
      return Stream.empty();
    }
    return Stream
        .generate(() -> {
            final T first = it.hasNext() ? it.next() : null;
            final T second = it.hasNext() ? it.next() : null;
            return first != null && second != null
                ? Pair.of(first, second)
                : null;
        })
        .takeWhile(x -> x != null);
  }

  /**
   * {@code [x₀, x₁, x₂, …] → [(x₀,x₁), (x₁,x₂), (x₂,x₃), …]} -
   * Converts an iterable to a stream of pairs of adjacent values.
   * Has the same effect as {@code zip(xs[:-1], xs[1:])} in Python.
   * @param xs an iterable of values [x₀, x₁, x₂, …].
   * @return {@code [(x₀,x₁), (x₁,x₂), (x₂,x₃), …]}.
   */
  public static <T> Stream<Pair<T,T>> streamAdjacent(Iterable<T> xs) {
    final Iterator<T> it = xs.iterator();
    // This iterator is basically just for keeping state.
    // A more elegant solution is certainly possible.
    final Iterator<Pair<T,T>> pairs = new Iterator<Pair<T,T>>() {
      T first = null;
      @Override
      public boolean hasNext() {
        throw new AssertionError("Should not be called!");
      }
      @Override
      public Pair<T,T> next() {
        if (first == null) {
            first = it.hasNext() ? it.next() : null;
        }
        final T second = it.hasNext() ? it.next() : null;
        if (first == null || second == null) {
            return null;
        }
        final Pair<T,T> result = Pair.of(first, second);
        first = second;
        return result;
      }
    };
    if (!it.hasNext()) {
      return Stream.empty();
    }
    return Stream
        .generate(pairs::next)
        .takeWhile(x -> x != null);
  }

  /**
   * {@code [(k₀:v₀), (k₁:v₁), (k₂:v₂), …] → [(k₀,v₀), (k₁,v₁), (k₂,v₂), …]} -
   * Converts a Map to a stream of key-value pairs.
   * Allows you to replace
   * {@code map.entrySet().stream().map(Pairs::pairFrom)}
   * with {@code stream(map)}.
   * @return a stream of key-value pairs [(k₀,v₀), (k₁,v₁), (k₂,v₂), …]
   * @since 1.1.0
   */
  public static <K,V> Stream<Pair<K,V>> stream(final Map<K,V> map) {
    if (map.isEmpty()) {
      return Stream.empty();
    }
    return map.entrySet().stream().map(Pairs::pairFrom);
  }

  /**
   * {@code [x₀, x₁, x₂, …] → [(0,x₀), (1,x₁), (2,x₂), …]} -
   * Converts an iterable to a stream of pairs where the first value is the
   * index of the value. Like {@code enumerate()} in Python.
   * Most useful with Lists so that its elements can be iterated together with
   * their indices.
   * <p>
   * The stream is constructed lazily. So the iterable could potentially return
   * an infinite iterator.
   * @param xs an iterable of values [x₀, x₁, x₂, …].
   * @return a stream of index-value pairs [(0,x₀), (1,x₁), (2,x₂), …].
   * @since 1.1.0
   */
  public static <T> Stream<Pair<Integer,T>> enumerate(final Iterable<T> xs) {
    final Iterator<T> it = xs.iterator();
    if (!it.hasNext()) {
      return Stream.empty();
    }
    return IntStream.range(0, Integer.MAX_VALUE)
        .boxed()
        .takeWhile(i -> it.hasNext())
        .map(withId(i -> it.next()));
  }

  /**
   * Returns a Collector that can collect a stream of pairs into a Map.
   * @since 1.1.0
   */
  public static <T1,T2> Collector<Pair<T1,T2>,?,Map<T1,T2>> pairsToMap() {
    return Collectors.toMap(Pair::get_1, Pair::get_2);
  }

  /**
   * {@code ([x₀, x₁, x₂, …], [y₀, y₁, y₂, …]) → [(x₀,y₀), (x₁,y₁), (x₂,y₂), …]}
   * - like {@code zip(xs, ys)} in Python.
   * @param xs an Iterable of values [x₀, x₁, x₂, …].
   * @param ys an Iterable of values [y₀, y₁, y₂, …].
   * @return a Stream of x-y pairs [(x₀,y₀), (x₁,y₁), (x₂,y₂), …].
   */
  public static <T1,T2> Stream<Pair<T1,T2>> zip(
          final Iterable<T1> xs,
          final Iterable<T2> ys)
  {
    final Iterator<T1> it1 = xs.iterator();
    final Iterator<T2> it2 = ys.iterator();
    if (!it1.hasNext() || !it2.hasNext()) {
      return Stream.empty();
    }
    return Stream
        .generate(() ->
            it1.hasNext() && it2.hasNext()
                ? Pair.of(it1.next(), it2.next())
                : null
        )
        .takeWhile(x -> x != null);
  }

  /**
   * {@code (f: x,y → z) → (g: (x,y) → z)} -
   * Converts a binary function to a function that is unary for pairs.
   * Allows {@code streamOfPairs.map(function(f))} instead of
   * {@code streamOfPairs.map(p -> f.apply(p._1, p._2))}.
   * @see ch.rfin.util.Pair#apply(BiFunction)
   */
  public static <T1,T2,R> Function<Pair<T1,T2>,R> function(
          final BiFunction<? super T1, ? super T2, ? extends R> f
    ) {
    return p -> f.apply(p._1, p._2);
  }

  /**
   * {@code (f: (x,y) → z) → (g: x,y → z)} -
   * Converts a unary function from pairs to a binary function.
   */
  public static <T1,T2,R> BiFunction<T1,T2,R> biFunction(
          final Function<Pair<T1,T2>, ? extends R> f
    ) {
    return (a,b) -> f.apply(Pair.of(a,b));
  }

  /**
   * {@code (f: x,y → T/F) → (g: (x,y) → T/F)} -
   * Converts a binary predicate to a predicate that is unary for pairs.
   * Allows {@code streamOfPairs.filter(predicate(f))} instead of
   * {@code streamOfPairs.filter(p -> f.test(p._1, p._2))}.
   * @see ch.rfin.util.Pair#test(BiPredicate)
   */
  public static <T1,T2> Predicate<Pair<T1,T2>> predicate(
          final BiPredicate<? super T1, ? super T2> f
    ) {
    return p -> f.test(p._1, p._2);
  }

  /**
   * {@code (f: (x,y) → T/F) → (g: x,y → T/F)} -
   * Converts a unary predicate from pairs to a binary predicate.
   */
  public static <T1,T2> BiPredicate<T1,T2> biPredicate(
          final Predicate<Pair<T1,T2>> f
    ) {
    return (a,b) -> f.test(Pair.of(a,b));
  }

  /**
   * {@code (f: x,y → _) → (g: (x,y) → _)} -
   * Converts a binary consumer to a consumer that is unary for pairs.
   * Allows {@code streamOfPairs.forEach(consumer(f))} instead of
   * {@code streamOfPairs.forEach(p -> f.accept(p._1, p._2))}.
   * @see ch.rfin.util.Pair#accept(BiConsumer)
   */
  public static <T1,T2> Consumer<Pair<T1,T2>> consumer(
          final BiConsumer<? super T1, ? super T2> f
    ) {
    return p -> f.accept(p._1, p._2);
  }

  /**
   * {@code (f: (x,y) → _) → (g: x,y → _)} -
   * Converts a unary consumer of pairs to a binary consumer.
   */
  public static <T1,T2> BiConsumer<T1,T2> biConsumer(
          final Consumer<Pair<T1,T2>> f
    ) {
    return (a,b) -> f.accept(Pair.of(a,b));
  }

  /**
   * {@code (f: x → y) → (g: x → (x,y))} - Takes a function and returns a
   * function to pairs, where the first element is the original input and the
   * second element is the output.
   * @param f a function f: x → y
   * @return a function g: x → (x,y)
   */
  public static <T,R> Function<T,Pair<T,R>> withId(
          final Function<? super T, ? extends R> f
    ) {
    return x -> Pair.of(x, f.apply(x));
  }

  /**
   * Returns a Comparator that compares pairs lexicographically.
   * <p>
   * <pre>
   * <code>
   * Collections.sort(pairsOfComparables); // ERROR, Pair does not implement Comparable.
   * Collections.sort(pairsOfComparables, Pairs.comparator()); // OK
   * </code>
   * </pre>
   * @since 1.1.0
   */
  public static
      <T1 extends Comparable<? super T1>,
       T2 extends Comparable<? super T2>>
      Comparator<Pair<T1,T2>> comparator()
  {
    return Comparator
      .comparing((Pair<T1,T2> p) -> p._1)
      .thenComparing((Pair<T1,T2> p) -> p._2);
  }

  /**
   * Compare two pairs lexicographically.
   * @since 1.1.0
   */
  public static
      <T1 extends Comparable<? super T1>,
       T2 extends Comparable<? super T2>>
      int compare(final Pair<T1,T2> p1, final Pair<T1,T2> p2)
  {
    return Pairs.<T1,T2>comparator().compare(p1, p2);
  }

  /**
   * a → (b → (a,b)) -
   * Takes an item and returns a function that will take a second item and
   * return a pair with the parameter to this function first and the parameter
   * to the returned function second.
   * This is essentially just the Pair factory method curried with the first
   * item.
   */
  public static <T1,T2> Function<T2,Pair<T1,T2>> pairWithFirst(final T1 first) {
    return s -> Pair.of(first, s);
  }

  /**
   * b → (a → (a,b)) -
   * Takes an item and returns a function that will take a second item and
   * return a pair with the parameter to this function second and the parameter
   * to the returned function first.
   */
  public static <T1,T2> Function<T1,Pair<T1,T2>> pairWithSecond(final T2 second) {
    return t -> Pair.of(t, second);
  }

  /**
   * {@code (f: x → y) → (g: (x,y) → (f(x),y))} -
   * Returns a function for mapping the first element in a pair.
   * @since 1.1.0
   */
  public static <T1,T2,R> Function<Pair<T1,T2>, Pair<R,T2>> map_1(
          final Function<? super T1, ? extends R> f)
  {
    return p -> p.map_1(f);
  }

  /**
   * {@code (f: x → y) → (g: (x,y) → (x,f(y)))} -
   * Returns a function for mapping the second element in a pair.
   * @since 1.1.0
   */
  public static <T1,T2,R> Function<Pair<T1,T2>, Pair<T1,R>> map_2(
          final Function<? super T2, ? extends R> f) {
    return p -> p.map_2(f);
  }

  /**
   * {@code (f: x → y), (g: u → v) → (h: (x,u) → (f(x),g(u)))} -
   * Returns a function for mapping both elements in a pair.
   * @since 1.1.0
   */
  public static <T1,T2,R,U> Function<Pair<T1,T2>, Pair<R,U>> map(
          final Function<? super T1, ? extends R> f,
          final Function<? super T2, ? extends U> g)
  {
    return p -> p.map(f, g);
  }

  /**
   * {@code (f: x → T/F) → (g: (x,y) → (f(x),y))} -
   * Returns a predicate for testing the first element in a pair.
   * @since 1.1.0
   */
  public static <T1,T2> Predicate<Pair<T1,T2>> test_1(final Predicate<? super T1> f) {
    return p -> f.test(p._1);
  }

  /**
   * {@code (f: x → T/F) → (g: (x,y) → (x,f(y)))} -
   * Returns a predicate for testing the second element in a pair.
   * @since 1.1.0
   */
  public static <T1,T2> Predicate<Pair<T1,T2>> test_2(final Predicate<? super T2> f) {
    return p -> f.test(p._2);
  }

}
