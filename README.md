# Pair

Tiny library consisting of a `Pair` class and a `Pairs` utility class that has
some extra methods for working with pairs.

It's designed with functional programming in mind. Pairs are immutable, and
they play nicely with the standard functional interfaces. Some of the
utilities and convenience methods are specifically intended to make pairs
convenient to use together with Java 8 Streams.

## License

Uses the MIT [license](LICENSE.txt).
